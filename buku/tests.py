from django.test import TestCase, Client
from django.urls import resolve
from .views import home, dataBuku

class Test(TestCase):
    def test_url_ada(self):
        response = Client().get('/buku/')
        self.assertEquals(response.status_code, 200)
        response = Client().get('/buku/dataBuku/?q=a')
        self.assertEquals(response.status_code, 200)
        
    def test_html_ada(self):
        response = Client().get('/buku/')
        self.assertTemplateUsed(response, 'buku/home.html')
        
    def test_menggunakan_fungsi_views(self):
        found = resolve('/buku/')
        self.assertEqual(found.func, home)
        found = resolve('/buku/dataBuku/')
        self.assertEqual(found.func, dataBuku)

    def test_judul(self):
        response = Client().get('/buku/')
        html_didapatkan = response.content.decode('utf8')
        self.assertIn("Daftar Buku", html_didapatkan)
        
    def test_json_response(self):
        response = Client().get('/buku/dataBuku/?q=web')
        html_didapatkan = response.content.decode('utf8')
        self.assertIn("web", html_didapatkan)
       
