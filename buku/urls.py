from django.urls import path


from . import views
from buku.views import dataBuku

app_name = 'buku'

urlpatterns = [
    path('', views.home, name='home'),
    path('dataBuku/', dataBuku)
]