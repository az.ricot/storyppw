$(document).ready(function() {
    
    functBuku("web");
    
    $(document).on('keyup','#inputBuku', function() {
        functBuku(null);
    });
    
    function functBuku(param) {
        if (param == null) {
            var input = $("#inputBuku").val();
        } else {
            var input = param;
        }
        console.log(input);
        
        $.ajax({
            url : 'dataBuku/?q=' + input,
            success : function(data) {
                var buku = data.items;
                console.log(buku);
                $("#isiBuku").empty();
                for (i = 0; i < buku.length; i++) {
                    var judulBuku = buku[i].volumeInfo.title;
                    var coverBuku = buku[i].volumeInfo.imageLinks.smallThumbnail;
                    var infoBuku = buku[i].volumeInfo.canonicalVolumeLink;
                    var previewBuku = buku[i].volumeInfo.previewLink;
                    
                    if (buku[i].volumeInfo.description != null) {
                        var deskripsiBuku = buku[i].volumeInfo.description;
                    } else {
                        var deskripsiBuku = "Tidak terdapat deskripsi untuk buku ini."
                    }
                    
                    $("#isiBuku").append(
                        "<tr>"
                            + "<td rowspan='3' class='coverBuku'>" + "<img src=" + coverBuku + ">" + "</td>"
                            + "<td class='judulBuku'>" + judulBuku + "</td>"
                        + "</tr>"
                        + "<tr class='deskripsiBuku'>"
                            + "<td>" + deskripsiBuku + "</td>"
                        + "</tr>"
                        + "<tr class='linkBuku'>"
                            + "<td>" + "<a class='link' href=" + infoBuku + ">Info</a> " + "<a class='link' href=" + previewBuku + ">Preview</a>" + "</td>"
                        + "</tr>"
                    );
                }
            }
        });
    }
});