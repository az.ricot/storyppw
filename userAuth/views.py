from django.shortcuts import render, redirect
from django.views import generic
from .forms import AccountCreationForm , AccountLoginForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login,logout

def signUp(request):

    form = AccountCreationForm(request.POST or None)
    if request.POST and form.is_valid():
        form.save()
        return redirect('userAuth:login')
    
    context = {'form' : form}
    return render(request, 'registration/signup.html', context)
    
def auth_login(request):
    
    form = AccountLoginForm(data=request.POST or None)
    if request.POST and form.is_valid():
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        
        if user is not None:
            login(request, user)
            return redirect('main:home')
    
    context = {'form' : form}
    return render(request, 'registration/login.html', context)
    
def auth_logout(request):
    logout(request)
    return redirect('main:home')