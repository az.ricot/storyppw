from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.forms.widgets import PasswordInput, TextInput
from django import forms

class AccountLoginForm(AuthenticationForm):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'placeholder':'Username', 'class':'form-control', 'autocomplete': 'off'})
        self.fields['password'].widget.attrs.update({'placeholder':'Password', 'class':'form-control'})
        
class AccountCreationForm(UserCreationForm):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'placeholder':'Username', 'class':'form-control', 'autocomplete': 'off'})
        self.fields['password1'].widget.attrs.update({'placeholder':'Password', 'class':'form-control', 'autocomplete': 'off'})        
        self.fields['password2'].widget.attrs.update({'placeholder':'Confirm Password', 'class':'form-control'})
        