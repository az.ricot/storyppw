from django.urls import path
from .views import *
from . import views

app_name = 'userAuth'

urlpatterns = [
    path('signup/', views.signUp, name='signup'),
    path('login/', views.auth_login, name='login'),
    path('logout/', views.auth_logout, name='logout'),
]
