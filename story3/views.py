from django.shortcuts import render

def home(request):
    return render(request, 'story3/home.html')

def projek(request):
    return render(request, 'story3/projek.html')

