from django.test import TestCase, Client
from django.urls import resolve
from .views import home, projek


class Test(TestCase):
    def test_url_ada(self):
        response = Client().get('/story3/')
        self.assertEquals(response.status_code, 200)
        response = Client().get('/story3/projek/')
        self.assertEquals(response.status_code, 200)
        
    def test_html_ada(self):
        response = Client().get('/story3/')
        self.assertTemplateUsed(response, 'story3/home.html')
        response = Client().get('/story3/projek/')
        self.assertTemplateUsed(response, 'story3/projek.html')
        
    def test_menggunakan_fungsi_views(self):
        found = resolve('/story3/')
        self.assertEqual(found.func, home)
        found = resolve('/story3/projek/')
        self.assertEqual(found.func, projek)

    def test_judul(self):
        response = Client().get('/story3/')
        html_didapatkan = response.content.decode('utf8')
        self.assertIn("Rico Tadjudin", html_didapatkan)
        
        response = Client().get('/story3/projek/')
        html_didapatkan = response.content.decode('utf8')
        self.assertIn("Projek", html_didapatkan)
