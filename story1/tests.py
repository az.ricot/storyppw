from django.test import TestCase, Client
from django.urls import resolve
from .views import home


class Test(TestCase):
    def test_url_ada(self):
        response = Client().get('/story1/')
        self.assertEquals(response.status_code, 200)
        
    def test_html_ada(self):
        response = Client().get('/story1/')
        self.assertTemplateUsed(response, 'story1/home.html')
        
    def test_menggunakan_fungsi_home(self):
        found = resolve('/story1/')
        self.assertEqual(found.func, home)

    def test_judul(self):
        response = Client().get('/story1/')
        html_didapatkan = response.content.decode('utf8')
        self.assertIn("Profil Saya", html_didapatkan)