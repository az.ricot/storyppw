from django.urls import path

from . import views

app_name = 'form'

urlpatterns = [
    path('', views.form, name='form'),
    path('submitForm', views.submitForm),
    path('view/', views.view, name='view'),
    path('matkul/<int:pk>', views.matkul, name='matkul'),
    path('hapus/', views.hapus, name='hapus'),
    path('konfirmasiHapus/<int:pk>', views.konfirmasiHapus, name='konfirmasiHapus'),
    path('hapusBeneran/<int:pk>', views.hapusBeneran, name='hapusBeneran'),
]