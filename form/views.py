from django.shortcuts import render, redirect
from .forms import InputFormMatkul
from .models import Matkul

def form(request):
    response = {'inputForm' : InputFormMatkul}
    return render(request, 'form/form.html', response)
    
def submitForm(request):
    formMatkul = InputFormMatkul(request.POST or None)
    if (formMatkul.is_valid() and request.method == "POST"):
        formMatkul.save()
    return redirect('form:form')

def view(request):
    matkuls = Matkul.objects.all()
    return render(request, 'form/view.html', {'matkuls':matkuls})
    
def matkul(request, pk):
    matkul = Matkul.objects.get(pk=pk)
    return render(request, 'form/matkul.html', {'matkul':matkul})
    
def hapus(request):
    matkuls = Matkul.objects.all()
    return render(request, 'form/hapus.html', {'matkuls':matkuls})
    
def konfirmasiHapus(request, pk):
    matkul = Matkul.objects.get(pk=pk)
    return render(request, 'form/konfirmasiHapus.html', {'matkul':matkul})
    
def hapusBeneran(request, pk):
    Matkul.objects.get(pk=pk).delete()
    return redirect('form:hapus')
    