from django.db import models

class Matkul(models.Model):
    nama = models.CharField(max_length=50)
    dosen = models.CharField(max_length=50)
    sks = models.IntegerField()
    deskripsi = models.TextField()
    semester_tahun = models.CharField(max_length=30)
    ruang_kelas = models.CharField(max_length=30)