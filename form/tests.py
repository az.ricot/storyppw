from django.test import TestCase, Client
from django.urls import resolve
from .views import form, submitForm, view, matkul, hapus, konfirmasiHapus, hapusBeneran
from .models import Matkul

class Test(TestCase):
    def test_url_ada(self):
        response = Client().get('/form-matkul/')
        self.assertEquals(response.status_code, 200)
        response = Client().get('/form-matkul/submitForm')
        self.assertEquals(response.status_code, 302) #302 untuk redirect
        response = Client().get('/form-matkul/view/')
        self.assertEquals(response.status_code, 200)
        response = Client().get('/form-matkul/hapus/')
        self.assertEquals(response.status_code, 200)
        
    def test_html_ada(self):
        response = Client().get('/form-matkul/')
        self.assertTemplateUsed(response, 'form/form.html')
        response = Client().get('/form-matkul/submitForm')
        self.assertRedirects(response, expected_url='/form-matkul/', status_code=302, target_status_code=200)
        response = Client().get('/form-matkul/view/')
        self.assertTemplateUsed(response, 'form/view.html')
        response = Client().get('/form-matkul/hapus/')
        self.assertTemplateUsed(response, 'form/hapus.html')
        
    def test_menggunakan_fungsi_views(self):
        found = resolve('/form-matkul/')
        self.assertEqual(found.func, form)
        found = resolve('/form-matkul/submitForm')
        self.assertEqual(found.func, submitForm)
        found = resolve('/form-matkul/view/')
        self.assertEqual(found.func, view)
        found = resolve('/form-matkul/hapus/')
        self.assertEqual(found.func, hapus)

    def test_judul(self):
        response = Client().get('/form-matkul/')
        html_didapatkan = response.content.decode('utf8')
        self.assertIn("Form", html_didapatkan)
        
        response = Client().get('/form-matkul/view/')
        html_didapatkan = response.content.decode('utf8')
        self.assertIn("Mata Kuliah", html_didapatkan)
        
        response = Client().get('/form-matkul/hapus/')
        html_didapatkan = response.content.decode('utf8')
        self.assertIn("Hapus", html_didapatkan)
        
        
    def test_model_bisa_membuat_objek(self):
        #Creating a new activity
        matkul1 = Matkul.objects.create(nama='PPW', dosen='Kak Pewe', sks='3', deskripsi='Kak Pewe buat Website', semester_tahun='Gasal 2020/2021', ruang_kelas='Rumah')

        #Retrieving all available activity
        jumlah_matkul = Matkul.objects.all().count()
        self.assertEqual(matkul1.nama, 'PPW')
        self.assertEqual(jumlah_matkul, 1)

        
    
