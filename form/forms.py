from django import forms
from .models import Matkul

class InputFormMatkul(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = ["nama", "dosen", "sks", "deskripsi", "semester_tahun", "ruang_kelas"]
    
    error_messages = {
        'required' : 'Informasi Wajib Diisi'
    }
    
    input_attrs_nama = {
        'type' : 'text',
        'placeholder' : 'Nama Mata Kuliah',
        'class' : 'form-control',
        'autocomplete' : 'off'
    }
    
    input_attrs_dosen = {
        'type' : 'text',
        'placeholder' : 'Dosen Pengajar',
        'class' : 'form-control',
        'autocomplete' : 'off'
    }
    
    input_attrs_sks = {
        'type' : 'number',
        'placeholder' : 'Jumlah SKS',
        'max_length' : '2',
        'class' : 'form-control',
        'autocomplete' : 'off'
    }
    
    input_attrs_deskripsi = {
        'type' : 'text',
        'placeholder' : 'Deskripsi Mata Kuliah',
        'class' : 'form-control',
        'autocomplete' : 'off'
    }
    
    input_attrs_semester = {
        'type' : 'text',
        'placeholder' : 'Semester Tahun (cth: Gasal 2020/2021)',
        'class' : 'form-control',
        'autocomplete' : 'off'
    }
    
    input_attrs_ruang = {
        'type' : 'text',
        'placeholder' : 'Ruang Kelas',
        'class' : 'form-control',
        'autocomplete' : 'off'
    }
    
    nama = forms.CharField(label='', required=True, max_length=50, widget=forms.TextInput(attrs=input_attrs_nama))
    dosen = forms.CharField(label='', required=True, max_length=50, widget=forms.TextInput(attrs=input_attrs_dosen))
    sks = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=input_attrs_sks))
    deskripsi = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=input_attrs_deskripsi))
    semester_tahun = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=input_attrs_semester))
    ruang_kelas = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=input_attrs_ruang))