from django.contrib import admin
from .models import Kegiatan, Peserta

admin.site.register(Kegiatan)
admin.site.register(Peserta)