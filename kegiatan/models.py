from django.db import models

class Kegiatan(models.Model):
    nama = models.CharField(max_length = 50)
    deskripsi = models.TextField()
    
class Peserta(models.Model):
    nama = models.CharField(max_length = 50)
    kegiatan = models.ForeignKey(Kegiatan, on_delete = models.CASCADE)