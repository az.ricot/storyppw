from django import forms
from .models import Kegiatan, Peserta

class FormKegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = ["nama", "deskripsi"]
    
    error_messages = {
        'required' : 'Informasi Wajib Diisi'
    }
    
    input_attrs_nama = {
        'type' : 'text',
        'placeholder' : 'Nama Kegiatan',
        'class' : 'form-control',
        'autocomplete' : 'off'
    }
    
    input_attrs_deskripsi = {
        'type' : 'text',
        'placeholder' : 'Deskripsi Kegiatan',
        'class' : 'form-control',
        'autocomplete' : 'off'
    }
    
    nama = forms.CharField(label='', required=True, max_length=50, widget=forms.TextInput(attrs=input_attrs_nama))
    deskripsi = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=input_attrs_deskripsi))


class FormPeserta(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = ["nama"]
        exclude = ('kegiatan',)
    
    error_messages = {
        'required' : 'Informasi Wajib Diisi'
    }
    
    input_attrs_nama = {
        'type' : 'text',
        'placeholder' : 'Nama Peserta',
        'class' : 'form-control',
        'autocomplete' : 'off'
    }
    
    nama = forms.CharField(label='', required=True, max_length=50, widget=forms.TextInput(attrs=input_attrs_nama))
