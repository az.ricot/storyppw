from django.test import TestCase, Client
from django.urls import resolve
from .views import kegiatan, submitFormKegiatan, tambahKegiatan
from .models import Kegiatan, Peserta

class Test(TestCase):
    def test_url_ada(self):
        response = Client().get('/kegiatan/')
        self.assertEquals(response.status_code, 200)
        response = Client().get('/kegiatan/tambah-kegiatan/')
        self.assertEquals(response.status_code, 200)
        response = Client().get('/kegiatan/tambah-kegiatan/submitFormKegiatan')
        self.assertEquals(response.status_code, 302) #302 untuk redirect
        
    def test_html_ada(self):
        response = Client().get('/kegiatan/')
        self.assertTemplateUsed(response, 'kegiatan/kegiatan.html')
        response = Client().get('/kegiatan/tambah-kegiatan/')
        self.assertTemplateUsed(response, 'kegiatan/tambahKegiatan.html')
        response = Client().get('/kegiatan/tambah-kegiatan/submitFormKegiatan')
        self.assertRedirects(response, expected_url='/kegiatan/tambah-kegiatan/', status_code=302, target_status_code=200)
        
    def test_menggunakan_fungsi_views(self):
        found = resolve('/kegiatan/')
        self.assertEqual(found.func, kegiatan)
        found = resolve('/kegiatan/tambah-kegiatan/submitFormKegiatan')
        self.assertEqual(found.func, submitFormKegiatan)
        found = resolve('/kegiatan/tambah-kegiatan/')
        self.assertEqual(found.func, tambahKegiatan)

    def test_judul(self):
        response = Client().get('/kegiatan/')
        html_didapatkan = response.content.decode('utf8')
        self.assertIn("Kegiatan-Kegiatan", html_didapatkan)
        
        response = Client().get('/kegiatan/tambah-kegiatan/')
        html_didapatkan = response.content.decode('utf8')
        self.assertIn("Tambah Kegiatan", html_didapatkan)
        
    def test_model_bisa_membuat_objek(self):
        kegiatan1 = Kegiatan.objects.create(nama = 'Asistensi PPW', deskripsi = 'Kegiatan ini adalah Asistensi untuk mata kuliah PPW')

        jumlah_kegiatan = Kegiatan.objects.all().count()
        self.assertEqual(kegiatan1.nama, 'Asistensi PPW')
        self.assertEqual(jumlah_kegiatan, 1)
        
        peserta1 = Peserta.objects.create(nama = 'Kak Pewe', kegiatan = kegiatan1)

        jumlah_peserta = Peserta.objects.all().count()
        self.assertEqual(peserta1.nama, 'Kak Pewe')
        self.assertEqual(peserta1.kegiatan, kegiatan1)
        self.assertEqual(jumlah_peserta, 1)
        