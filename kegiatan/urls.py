from django.urls import path

from . import views

app_name = 'kegiatan'

urlpatterns = [
    path('', views.kegiatan, name='kegiatan'),
    path('tambah-kegiatan/submitFormKegiatan', views.submitFormKegiatan),
    path('tambah-peserta/submitFormPeserta', views.submitFormPeserta),
    path('tambah-kegiatan/', views.tambahKegiatan, name='tambahKegiatan'),
    path('tambah-peserta/<int:pk>', views.tambahPeserta, name='tambahPeserta'),
    path('hapusKegiatan/<int:pk>', views.hapusKegiatan, name='hapusKegiatan'),
    path('hapusKegiatanBeneran/<int:pk>', views.hapusKegiatanBeneran, name='hapusKegiatanBeneran'),
]