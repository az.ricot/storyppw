from django.shortcuts import render, redirect
from .forms import FormKegiatan, FormPeserta
from .models import Kegiatan, Peserta

def kegiatan(request):
    semuaKegiatan = Kegiatan.objects.all()
    semuaPeserta = Peserta.objects.all()
    context = {
        'kegiatans' : semuaKegiatan,
        'pesertas' : semuaPeserta
    }
    return render(request, 'kegiatan/kegiatan.html', context)

def tambahKegiatan(request):
    response = {'form' : FormKegiatan}
    return render(request, 'kegiatan/tambahKegiatan.html', response)
    
def submitFormKegiatan(request):
    form = FormKegiatan(request.POST or None)
    if (form.is_valid() and request.method == "POST"):
        form.save()
    return redirect('kegiatan:tambahKegiatan')
    
def tambahPeserta(request, pk):
    objekKegiatan = Kegiatan.objects.get(pk=pk)
    response = {
        'form' : FormPeserta,
        'kegiatan' : objekKegiatan
    }
    return render(request, 'kegiatan/tambahPeserta.html', response)    
    
def submitFormPeserta(request):
    pk = request.POST.get('kegiatan_pk')
    objekKegiatan = Kegiatan.objects.get(pk=pk)

    form = FormPeserta(request.POST or None)
    if (form.is_valid() and request.method == "POST"):
        savedForm = form.save(commit=False)
        savedForm.kegiatan = objekKegiatan
        savedForm.save()
    return redirect('kegiatan:kegiatan')

def hapusKegiatan(request, pk):
    objekKegiatan = Kegiatan.objects.get(pk=pk)
    semuaPeserta = Peserta.objects.all()
    response = {
        'kegiatan' : objekKegiatan,
        'pesertas' : semuaPeserta
    }
    return render(request, 'kegiatan/hapusKegiatan.html', response)
    
def hapusKegiatanBeneran(request, pk):
    Kegiatan.objects.get(pk=pk).delete()
    return redirect('kegiatan:kegiatan')